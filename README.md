#Jz-SDL

SDL-1.2.15的介绍请见[这里](http://www.libsdl.org)  
这个项目是基于SDL-1.2.15进行的修改，以便使之更加适应我们的平台。

1. 添加了对eventX鼠标设备节点的支持
2. 修改了video/fbcon/代码，使之适应嵌入式平台的LCD屏
	1. 嵌入式平台,显示驱动使用的是framebuffer驱动
	2. 对于LCD屏来说，分辨率和显示模式都是固定的
	3. SDL的fbcon处理逻辑并未考虑到嵌入式平台,考虑的是vga输出
